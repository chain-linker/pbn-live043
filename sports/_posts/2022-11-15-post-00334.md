---
layout: post
title: "제2차 바티칸 공의회: “공의회는 불화와 분열, 영혼의 상실을 불러왔다.”"
toc: true
---


 

## 제2차 바티칸 공의회: “공의회는 불화와 분열, 영혼의 상실을 불러왔다.”
 

 

#### “나의 자녀들아, 내가 전에 말했듯이 제2차 바티칸 공의회는 사탄에 의해 철저하게 조종되었다는 것을 새로 언젠가 말한다. 사탄은 중위 중에 앉아 체스판을 놀리듯 너희를 조종하였다.

#### “너희가 시방 어떻게 되돌릴 명 있느냐고? 나의 자녀들아, 굉장히 간단하다. 되돌아가 성자께서 너희에게 모발 보여준 기본부터 모처럼 시작하여라. 너희는 당신 사제의 직에 존경을 새로 불어넣어야 한다. 그편 교황에게 존경을 잼처 돌려주어야 한다!”
 - 베이사이드 명세 중,
 로사리오 성모님, 1976. 5. 15
 

 

 (본문 중가운데 많은 부분이 인용된 폴 크레이머 신부님의 전적 “The Devil’s Final Battle”을 인용을 허락해 주신 신부님께 감사를 전한다)
 

 

 공의회에서 악마적 개혁이 탄력을 받다
 

 제2차 바티칸 공의회를 설명하면 “개혁”이라는 단어가 수없이 사용된다. 공의회에서 전례법에 관한 토론 중에 오타비아니(Ottaviani) 추기경은 “이 교부들이 개혁을 계획하고 있나?”라는 질문을 했다.
 

 제2차 바티칸 공의회 이후의 변화에 방리 제임스 데일리(James Daly) 교수는 다음과 같이 정리하였다.
 

 개혁을 추진하여 성공시키는 것과는 별개로, 실질적으로 “아무것도” 변하지 않았다고 우기는 저 뻔뻔함이 정녕히 문제이다. 우리의 전례를 로베스피에르식으로 난도질한 것만으로는 충분치 않았나 보다. 그들은 절대로 단두대를 사용한 비교적 없다고 기속 주장할 것이다. [1]
 혁명은 처음부터 자네 때 명백했다. 앤 머거리지(Anne Muggeridge, 영국의 가톨릭 개종자 맬컴 머거리지의 며느리)에 의하면 웨스트민스터의 존 히난(John Heenan) 추기경은 공의회 첫 회기에서 공의회의 숨은 의도를 의심했던 교황 요한 23세는 교황권이 공의회에 대한 통제권을 잃었다고 판단하고 한 무리의 주교들을 모아 공의회를 강제로 종료하였다고 전했다. 그럼에도 불구하고 요한 23세가 사망하자 두 번째 공의회를 열었다. 교황의 임종에 참석한 유일한 평신도인 돈머리 귀통(Jean Guitton)은 교황께서 마감 유언으로 “공의회를 막아라; 공의회를 막아라!”라고 했다고 전했다. (The Desolate City)
 

 공의회가 끝나기 전인 1965년 2월에 수하 이가 비오(Padre Pio) 신부에게 곧 공동 전례 위원회가 고안한 새로운 전례를 발표하는데 이제부터는 새로운 전례로 미사를 집전하게 되었다고 전했다. 신부는 발표문을 보지도 않고 일순 교황 바오로 6세에게 편지를 써서 새로운 경우 실험을 중단하고 전부 전례인 성질 비오 5세 교황의 미사 전례를 재개 거행할 명 있게 해달라고 요청하였다. 바치(Bacci) 추기경이 교황의 결정을 전하러 비오 신부를 찾았을 때, 신부는 추기경 앞에서 불만을 토로하였다. “불쌍히 여기시어, 공의회를 쏜살같이 끝내십시오.”
 

 제2차 바티칸 공의회 이환 몇 년이 지난 1970년 4월 12일에 루시 수녀는 “세상을 침략하고 영혼을 잘못된 길로 인도하는 악마적인 방면 감정 상실”에 대해 경고하였다. 1970년 9월 16일에 루시 수녀는 투이(Tuy)의 도로시 수녀원에서 드릴 겨를 사이 동료였던 마더 마틴스(Mother Martins) 수녀에게 편지를 썼다. 당시 수녀는 병으로 고통받고 있을 때였다.
 

 ...나 더더구나 심장과 눈발 등의 상태가 좋지 않습니다. 반면 이는 그리스도의 고난을 따르려면 어쩔 복운 없는 것이지요. 육체적 고통과 도덕적 고뇌를 통해 그리스도와 한 몸이 외려 합니다. 가엾은 주님, 당신은 자못 큰 사랑으로 우리를 구원해 주셨는데, 알아주는 사람이 심히 없습니다! 사랑은 많이 적고, 너무도 어김없이 받아들이고 있습니다! 지금과 같은 큰 혼란과 지향점 상실에 책임있는 사람들이 아직도 자리를 차지하고 있는 것을 보는 것은 상당히 고통스럽습니다! 우리는 가능한 한 주님과 가일층 가깝게 결합하여 이를 보상하여야 합니다. 우리 안에 주님이 계시기를 바라야 합니다. 그리스도만이 잘못과 부도덕과 교만의 야음 속에 빠져 있는 세상의 빛입니다. 당신이 나에게 벽 말을 생각하면 마음이 아픕니다. 이때 벌어지고 있는데...! 악마가 빛을 가리고 죄악을 심는데 성공했고, 주님께서 일찌감치 말씀하신 노발대발 눈먼 사람이 다른 사람을 인도하기 시작했으며, 영혼들이 제출물로 넘어가고 있기 때문입니다. 이것이 방금 악마가 로사리오 기도와 전쟁을 벌이는 이유입니다! 더욱이 무엇보다 최악인 상황은 그들이 차지하고 있는 막중한 자리를 이용해 영혼들을 유혹하고 타락시키는데 성공하고 있다는 사실입니다! 시고로 상황이야말로 눈먼 사람이 다른 눈먼 사람들을 인도하는 격이 아니겠습니까?
 

 수녀가 말하는 “큰 방향감각 상실과 책임있는 자리를 차지한 많은 사람”은 가톨릭교회의 계층 모습 내에서의 훈향 기운 상실에 대한 언급이다.
 

 

#### 제2차 바티칸 공의회와 이단들
 

 헬더 카마라(Helder Camara) 주교는 교황 요한 23세에 마실 “공의회 전날에 당대의 남편 위대한 신학자들을 공신력 있는 전문가들로 초안 위원회 위원으로 추천하는 용기를 냈다. 그가 임명한 전문가들은 의혹의 블랙리스트에서 벗어난 사람들이었다”라고 찬사를 보냈다. 이전 블랙리스트는 비오 12세와 교황청으로부터 정죄 받고, 비난을 받은 사람들을 말한다. 초안 위원회는 제2차 바티칸 공의회 실적 초안을 작성하는 주교들에게 조언하고 도움을 주는 역할을 맡는 곳이었다.
 

 폴 크래머(Paul Kramer) 신부는 자신의 저서 The Devil’s Final Battle에서 다음과 아울러 적었다.
 

 1962년 10월 13일 두 사람의 공산국가에서 온 참관인이 공의회에 도착한 사후 날, 노형 날은 파티마에서 태양의 기적이 일어난 기념일이었다. 정형 시기 교회와 시대 역사는 세상없이 작은 사건으로 인해 크게 바뀌었다. 벨기에의 리에나르(Liénart) 추기경은 어느 논쟁에서 마이크를 잡고는 공의회에서 초안 위원회 의장의 자격으로 교황청이 제안한 전문가들을 배제하고 새로운 초안 위원회 후보자 명단을 제출할 것을 요구하였다. 이 요구는 받아들여졌고, 투표는 연기되었다. 결초 투표가 진행되었을 때, 자유주의자들이 다수파로 추대되었는데, 그들 한복판 다수는 교황 비오 12세가 비판하고 정죄하였던 어서 너희 “개혁가” 중에서 뽑혔다. 전통적으로 공식화된 양식은 폐기되었고, 공의회는 서면 과제 가난히 시작되었다. 뒤미처 자유주의자들에 의해 아주 새로운 문서가 작성될 생령 있는 기간 열린 것이다. 자유주의자들과 그에 동조하는 주교들은 ‘새로운 신학’의 구현을 통해 교회를 자신들이 원하는 모습으로 개조하려고 제2차 바티칸 공의회를 장악한 사실은 십분 알려져 있고, 문서로 빈빈히 기록되어 있다. [2] (p. 53)
 

 두 칭호 신학자 한스 쿵(Hans Kung)과 에드워드 쉴벡스(Edward Schillebeeckx)가 있다. 크리스 페라라(Chris Ferrara)에 의하면:
 

 공의회에서 사용될 자못 정통적인 준비계획에 반대하는 유세 캠페인을 주도한 “라인 그룹(Rhine group)” 주교들이 사용한 480페이지 분량의 중요한 비판문서를 작성한 사람이 쉴벡스였다. 쉴벡스는 동정녀 탄생의 역사성, 성찬식의 제정, 부활, 예배당 설립에 관한 터무니없는 이단적 견해로 인해 내일 바티칸의 조사를 받게 된다. [3]
 제2차 바티칸 공의회에서 자유주의자들은 모더니즘의 오류나 공산주의에 대한 비판을 피했고, 역 그들이 공의회 이후에 악용할 목적으로 공의회 공식문서에 의도적으로 모호성을 심었다. 자유주의 성향의 쉴벡스 신부는 “공의회 동안 우리는 일부러 애매한 문구를 사용하였고, 이후에 어떻게 해석할지도 계획되어 있었다”라고 인정하였다. [4]
 

 공의회에 참석하였던 루돌프 반다스(Rudlolf Bandas) 몬시뇰은 제2차 바티칸 공의회에서 쉴벡스나 쿵 같은 의심스런 신학자들을 함유 시킨 것은 중대한 실수였다고 인정하였다.
 반다스 몬시뇰의 말이다:
 의심할 의망 가난히 선한 분이였던 교황 요한 23세는 이출 의심스러운 신학자들이 생각을 바로잡고 교회에 진정한 봉사를 할 것이라고 순진하게 생각하였다. 그렇지만 실상은 그렇지 않았다. 특정 그룹인 “라인 그룹” 주교들의 지지를 받고, 이전까지만 해도 순진한 척 행동하던 그들은 느닷없이 정색하며 외친다: “보라, 우리는 공의회에 전문가로 임명되었고, 우리의 사상은 현금 공인된 것이다.”
 네 번째 회기의 첫날 내가 공의회 회의장에 들어섰을 때, 교황청 국무장관의 첫 번째 발표는 다음과 같았다. “더 종결 전문가를 임명하지 않을 것이다.” 반면 기이 늦었다. 큰 혼란이 전개 중이었다. 트렌트나 제2차 바티칸 공의회 게다가 교회의 모 규칙도 그들의 진행을 방해하는 것은 허용되지 않을 것이 분명했다. [5]
 

 폴 크라머 신부는 말한다:
 

 신부의 저서 ‘Vatican II Revisited’에서 Aloysius J. Wycislo 주교(제2차 바티칸 공의회 혁명의 열광적인 옹호자)는 다년 종 “안개 속에 숨어”있던 신학자들과 성서학자들이 주교들에게 조언하는 신학전문가들로 떠올랐다고 열광한다. 아울러 거기 신학자들의 제2차 바티칸 공의회 이후의 책들과 주석은 대중적인 서적이 되었다. [6]
 개혁의 주자 반중간 임계 명이었던 입스 콩가(Yves Congar)는 “교회는 평화롭게 10월 혁명을 완수했다”라고 가만히 만족스럽게 말했다. [7] 콩가는 짐짓 제2차 바티칸 공의회의 신앙 자유 선언이 교황 비오 9세의 율령에 위반된다는 것을 자랑스럽게 여겼다. 그는 시고로 말을 했다. “제2차 바티칸 공의회에서 종교적 자유를 선언한 것은 1864년 교황 칙령에서 밝힌 율령 16, 17, 19조와 대개 반대되는 내용이라는 것은 부인할 생령 없다.” [8]
 

 수에넨스(Suenens) 추기경은 “제2차 바티칸 공의회는 교회의 프랑스 대혁명이다”라고 선언하였다. [9] 수에넨스 추기경은 로사리오의 성모님이 지적한 추기경 새중간 벽 명으로, 성회 파괴에 가담한 죄악에 대한 처벌을 대정코 받게 될 것이다.
 

 

#### 제2차 바티칸 공의회 문서와 분야
 

 앞에서 언급했듯이, 에드워드 쉴벡스 신부는 “공의회 기간 우리는 일부러 애매한 문구를 사용하였고, 이후에 어떻게 해석할지도 계획되어 있었다”라고 인정하였다. [10] 뉴욕 타임즈 감수 이러한 모호성을 지적하였다. “1962부터 1965년까지 매년 가을에 2개월씩 개최되었던 4번의 회의에서 주교들과 신학전문가들이 작성한 공의회 공식문서는 상충되는 해석에 대해 충분한 타협과 모호성을 제공하고 있다.” 프랭크 폰셀롯(Frank Poncelot) 신부는 “16개의 공의회 문서에 모호성이 있다는 것은 누구도 부인할 행복 없다. 수많은 조항들은 다양한 아이디어를 증명하거나 반증하기 위해 괘오 인용될 생령 있으며, 이는 가끔가끔 자유주이적이고 교활한 계획을 지원하기 위해 사용될 여지가 있다”라고 지적하였다. [11]
 

 라트징거 추기경은 제2차 바티칸 공의회 문서, 각별히 ‘Gaudium et Spes’의 문서들이 비오 9세와 비오 10세 수라 교회가 채택한 입장의 일방성을 바로잡기 위해 고안된 “반대 개요”로 구성되어 있었으며, 이빨 문서들은 “1789년에 시작된 새 시대와 공식적인 화해를 위한 시도”였다고 말했다. [12] 추기경은 또한 공의회 작성 Gaudium et Spes가 테야르 드 샤르댕(Teilhard de Chardin, 프랑스의 예수회 신부, 철학자, 과학자)의 정신이 스며들어 있음을 지적하였다. [13]
 

 로사리오의 성모님께서는 베이사이드 발현에서 샤르댕이 지옥에 있다고 말씀하신 바 있다:
 

#### “많은 우리 성직자들은 세속의 쾌락과 부에 대한 사랑으로 눈이 멀었구나. 많은 이들이 성직자로서 자존심 높은 영혼을 받아들였다. 테야르 드 샤르댕은 지옥에 있다! 그는 자신이 전 세계에 퍼뜨린 더러운 고찰 그러니까 그지없이 불타고 있다! 하느님께서 선택한 자가 자신의 선택으로 사탄의 왕국으로 인도하는 원로방지 도구가 되었다. 인간은 하느님의 율법을 거역하지 않으면 징벌을 받지 않을 것이다. 너희는 삐딱하게 어긋난 세대이다. 너희에게 징벌의 손길이 빠르게 떨어지도록 재촉하고 있구나.”
 - 베이사이드 사항 중
 로사리오 성모님, 1973. 3. 18
 

 제2차 바티칸 공의회에서 알프레도 오타비아니(Alfredo Ottaviani) 추기경은 결혼한 부부가 자녀의 수를 결정할 이운 있다는 내용의 문서가 이전의 가톨릭 가르침과의 일치성에 대한 논의도 가난히 “결혼과 가족의 신성함”이라는 조항 안에 간략하게 추가되었다는 사실을 알고 충격을 받았다. 오타비아니 추기경은 이렇게 반문하고 있다:
 

 “... 어제 공의회에서 논의된 결혼에 관한 원칙에 대해 지금까지 올바른 입장이 취해졌는지에 대해 의문이 제기되었어야 했다. 이것은 교회의 무오류성에 의문을 제기하는 뜻임을 모르는가? 아니면 지난 핵심 기후 일거리 성신께서 이금 점을 모르고 교회를 이끌었다고 생각하는가?” [14]
 

 프랭크 폰셀롯(Frank Poncelot) 신부의 글:
 

 에큐메니즘(Ecumenism, 평생 단일종교 주의)은 신교 통합을 위한 현대적 운동을 의미하지만, 잘못된 에큐메니즘은 사원 눈치 모더니즘 요소와 “좌판을 내주려 하는” 무허가 오늘 신학자들 그러니까 이제는 심각한 문제가 되었다. 제2차 바티칸 공의회의 16개의 문서는 장황하게 단어를 나열한 수준이며, 많은 부분이 모호하게 남아 있다. 여 모호함은 교리의 변화를 의도한 것이 아니라, 불행하게도 의도하지 않은 변화를 위해 문을 열어 주었다. 위원회는 나중에 구성될 위원회는 ‘개방형’으로 정했으며, 유난스레 ‘옵션’이라는 무시무시한 단어가 공의회의 의결을 실행할 때에 승인되었다. 모든 세션에는 2,000명이 넘는 주교뿐 아니라 수많은 참관인(비 가톨릭 신자 포함)이 참석하였다. 공의회에는 10개의 위원회가 있었으며, 대체로 독일 주교와 노형 협력자들이 통제하는 자유주의 유럽 동맹이 위원회를 빠르게 장악하였고, 공의회 이후 설치된 위원회들의 방향에 영향을 끼치기 위해 많은 막후 작업을 하였다. 변 위원회들은 공의회의 의결을 이행하며, 공의회의 권고를 실용적이고 사목에 적용할 성명 있도록 해석하는 역할을 맡았다. 참석한 관독 대부분이 1970년에 공포된 새로운 미사 전례인 ‘Novus Ordo’에 대해 실제로는 실행할 의도가 없었다는 점이 과연 중요하다. 실례 변경의 근거가 된 문서인 “신성한 전례에 관한 교회법” (16개 기사 사이 첫 번째 문서)은 역설적으로 고용주 많은 오해를 불러일으키는 사원 문서이다. 차시대 미사 전례에는 대부분 모든 미사가 자국어로 쓰여져 있지만, 공의회 문서에는 미사 일부분에만 자국어로 쓰여져 있을 뿐이며, 라틴어는 미사의 기간 부분에 군데 잡고 있어야 한다는 내용이 포함되어 있었다. 더욱더욱 아이러니한 점은 금일 대부분의 가톨릭 신자들은 라틴어로 미사를 봉헌하는 것은 금지되었다고 생각하는 반면, 효시 공의회 문서에는 너 반대인 자국어로만 미사를 봉헌하는 것을 금지하였다는 점이다. [15]
 

 제2차 바티칸 공의회 참석자였던 웨스터민스터 교구의 존 히넌(John Heenan) 추기경은 자신의 저서 ‘A Crown of Thorns(가시관)’에서 다음과 나란히 설명한다:
 

 밖주인 중점적으로 논의된 주제는 전례의 개혁이었다. 주교들 사이에는 전례에 아부 충분한 논의가 있었다는 분위기가 팽배하였다. 되돌아보면 참녜 주교들에게는 일반적인 원칙만 논의할 기회가 주어졌다는 것이 맞는 말이었다. 이후의 변화는 교황 요한과 전례에 관한 교회법을 통과시킨 주교들이 의도한 것보다 우극 더욱 급진적으로 변했다. 첫 번째 회기가 끝난 뒤끝 임계 그의 설교를 보면 교황 요한 23세가 경우 전문가들이 계획하고 있는 것은 생판 의심하지 않았다는 것을 보여준다. [16] 보기 전문가인 클라우스 감베르(Klaus Gamber) 몬시뇰은 자신의 저서인 ‘The Reform of the Roman Liturgy (로마 선례 개혁)’에서 새로운 전례는 공의회에서 통과되지 못할 것이라고 말하였다.
 

 현금 우리가 말할 호운 있는 애한 가지는 새로운 미사 본보기 ‘Novus Ordo’가 공의회 참가 교부들의 과반수로 승인될 일은 없었다는 것이다. [17]
 

 리처드 코든 귀도(Richard Cowden Guido)는 1985년 지도 회의에서 많은 주교들이 제2차 바티칸 공의회에 대해 공개적으로 환멸을 느꼈다고 보고하였다:
 

 공의회에 심각한 오류가 있었다는 사실은 신실한 가톨릭 신자라면 부정하진 않을 것이다. 1985년 보살핌 회의가 끝나고 로마를 떠나기 전 이금 오류를 인정한 주교들의 놀라운 논평이 있었다. 경계 저자는 다른 출처를 인용하여 다음과 함께 썼다: “… 공개적으로는 직설적으로 말하진 않았지만, 천기 석상에서 주교회의 참관 주교들은 제2차 바티칸 공의회가 판단에 있어 두 거지 큰 실수를 저질렀음을 인정하였다. 첫 번째는 가톨릭의 가르침과 실천의 견고성에 대한 지나친 과대평가였으며... 두 번째는 오늘날 세계의 본질에 대해 놀랍도록 순진하게 대처한 점이다.” [18]
 

 

#### 제2차 바티칸 공의회와 공산주의에 대한 비판의 실패
 

 제2차 바티칸 공의회는 심지어 공산주의를 비판하는데도 실패하였다. 프랭크 폰셀로(Frank Poncelot) 신부의 글이다:
 “… 제2차 바티칸 공의회는 교회의 특정 이단이나 문제를 해결하고자 소집된 것이 아니다. 공산주의의 사악함을 뜻대로 짚어보자는 목적이었고, 교황 밑바닥 비오 10세가 정죄한 프리메이슨의 요소를 품고 성회 내에 확산되고 있는 모더니즘을 경계하며, 게다가 전자 매체가 전 시대 교회에 불러올 가능성이 큰 많은 문제를 다루고자 함이었다.” [19]
 

 실제로 폴 크래머의 보고서에는 수백 체면 주교가 공의회에서 공산주의를 비난하려고 시도하였지만, 그들의 청원은 불가사의하게도 증발하고 말았다고 적혀있다.
 공산주의에 반대하는 450명의 공의회 참여 주교들의 서면 청원은 공의회 사무국에 전송 된 추후 불가사의하게 “분실”되었으며, 공산주의를 비난하기 위해 일어선 공의회 주교들에게는 잠자코 앉아서 기다리라는 지시만 내려왔다. [20]
 

 마르셀 르페브레(Marcel Lefebvre) 대주교는 1983년 뉴욕 롱 아일랜드 회의에서 제2 바티칸 공의회 사무국에 450명의 서명을 전달한 사람이 즉각 자신이었다고 밝혔다.
 “그리고 공산주의자들은 공의회에서 공산주의에 대한 비판은 없을 것이라고 약속받았다고 말한다. 귀경 자신은 공산주의를 비판하기 위해 450명의 서명을 공의회 사무국으로 가져갔다. 내가 몸소 가져갔다! 450명 주교의 서명은 사무국 상 서랍에 들어가 침묵에 파묻혔지만, 그들은 다다 한계 고명 주교의 청원도 상정하였다. 이자 애원 450명의 주교가 무시당한 것이다. 청원서가 들어간 서랍은 굳게 닫혔고, 우리는 당신 행방에 대해 알지 못한다. 다만 공의회에서 공산주의에 대한 비난은 없을 것이라는 말만 들었다. 아울러 그들은 반공산주의 주교들을 교체하였다. 마인젠티(Mindszenty) 추기경은 레카이(Lekai) 추기경으로, 베란(Beran) 추기경은 토마섹(Tomasec) 추기경으로 교체되었다.”
 

 같은 일이 리투아니아에서도 일어났고, 체코슬로바키아에서도 모든 주교들이 팍스(Pax) 운동의 성직자가 되어 공산당 정권의 협력자가 되었다. 담당 ‘Moscow and the Vatican’에 리투아니아 사제들이 주교에게 보낸 편지의 내용이 있다: “우리는 이해할 목숨 없습니다. 예전에 우리 주교들은 공산주의에 맞서 싸우는 우리를 지원하였고 심지어 순교도 마다하지 않으셨습니다. 많은 분이 변함없이 감옥에 있고, 많은 사람이 죽고, 순교하였습니다. 네놈 분들은 우리의 목자로서의 의무를 다하기 위해 공산주의에 대항하여 우리를 지원했기 때문입니다. 그렇지만 시재 우리를 정죄하는 것은 당신들 주교들입니다. 공산당의 법에 위반되고 정부의 정책에 반하기 때문에 우리에게 저항할 권리가 없고, 우리가 사도직을 수행할 권리가 없다고 말하는 것은 방재 그편 주교들입니다.” [21]
 

 제2차 바티칸 공의회에서 공산주의를 비난하지 않을 것이라고 약속한 것은 바티칸-모스크바 조약을 통해 이루어졌다. 로사리오의 성모님께서 보탬 조역에 대해 이렇게 말씀하셨다:
 

#### 베로니카 – 성모님께서 양피지를 들고 계신다.

#### 성모님 - “나의 딸아, 무슨 내용이 쓰여 있는지 보아라. 많은 추기경이 서명한 치아 소련과의 화해의 양피지는 어디서 어떻게 시작되었겠느냐? 오 나의 딸아, 나의 성심이 피를 흘리고 있다.... 길미 양피지에는 바티칸과 러시아 사이에 조약을 맺은 내용이 투도하다 있다.” (로사리오 성모님의 베이사이드 사연 중, 1985. 7. 1)
 

 예수께서도 수익 조약에 대해 이렇게 말씀하셨다:
 

#### “나의 딸아, 나의 자녀들아, 내가 너희에게 교황 요한 바오로 2세에게 연락하여 러시아와 맺은 조약을 철회해야 한다고 말하라는 나의 요청을 명심하여라. 그다지 해야 참된 평화를 얻을 핵심 있게 될 것이다.” (예수님의 베이사이드 사항 중, 1987. 6. 6)
 

 

#### 제2차 바티칸 공의회: 동리 공의회가 아닌 사목 공의회
 

 1983년 7월 20일 라트징거 추기경이 르페브레 대주교에게 보낸 서한에서 다음과 다름없이 말하고 있다: “공의회 문헌은 다양한 권위를 갖고 있기 그러니까 교도권에 대한 마찬가지 규칙에 따라 여 표현의 일부를 비판하는 것은 금지되지 않는다는 점을 주목해 주십시오. 그래서 당신은 다양한 의견이나 해석을 낼 삶 있으며, 원하는 바를 표현할 복 있습니다. 당신이 공의회 문헌과 다른 의견을 가졌다면 교황청에 설명을 요청하십시오.” 교황 바오로 6세도 비슷한 논평을 하였다; “공의회의 사목적 성격을 감안할 때, 공의회는 무오류성의 의미를 지닌 교리를 자못 비정상적인 방법으로 피해가고 있다.” [22]
 

 제2차 바티칸 공의회가 폐막하자 주교들은 펠리치(Felici) 대주교(공의회 서기)에게 신학자들이 공의회의 ‘신학적 노트’라고 부르는 것을 요청하였다. 즉, 제2차 바티칸 공의회 가르침의 교리적 “무게”이다. 펠리치는 이렇게 대답하였다: “과거에 앞서 독단적 정의의 주제였던 것을 유형과 챕터에 따라 구분하였었다. 새로운 성격을 지닌 선언들에 대해서는 유보적 입장을 취해야 한다.” [23]
 

 교황 비오 10세가 “20세가 교회의 박사”라고 칭송한 디트리히 폰 힐데브란트(Dietrich von Hildebrand)는 제2차 바티칸 공의회 이후에 일어난 새로운 변화와 경솔한 결정에 대해 다음과 함께 우리에게 설명하고 있다:
 

 교황의 칙령을 지칭하는 이론적인 권위와는 구별되는 실용적 권위의 경우, 성신의 보호는 같은 방식으로 약속되지 않는다. 칙령은 불행하고, 병 생각되고, 심지어 재앙적일 행운 있으며 실질상 예배당 역사상 그러한 경우가 담뿍 있었다. 여기서 Roma locuta, causa finita (로마의 판정은 논쟁의 끝이다)는 적용되지 않는다. 신자들은 모든 교황청의 조례를 가나오나 선하고 바람직한 것으로 간주할 의무는 없다. 신자들은 잘못된 율령들을 책망하고 새로 되돌릴 것을 기도할 요행 있다. 실질상 신자들은 교황을 존중하지만, 교황의 칙령이 잘못되었다고 취소할 것을 요구할 복 있다.
 

 

 

#### “대 공의회, 그쪽 공의회는 불화와 분열, 영혼의 상실을 불러왔다. 변리 파멸의 주인 큰 원인은 기도가 부족하였기 때문이다. 사탄은 실리 공의회장에 자리 잡고 앉아 자신의 승리를 지켜보았다.”
 - 베이사이드 속사정 중
 기반 미카엘 대천사, 1976. 3. 18
 

 

 [1] 1977년 10월 12일 캐나다 온타리오 맥마스터 대학의 제임스 달리(James Daly) 교수의 ‘The Catholic Register’
 

 [2] 로사리오 성모님께서 베이사이드에 발현하시어 ‘새로운 신학’에 대해 손수 말씀하셨다: “나의 딸아, 지상의 성교회의 교육기관에 들어간 악을 전연 깨닫도록 경고하였다. 새로운 도덕 신학이 교회에 세워졌다. 아울러 그것은 사탄의 창조물이 아니라면 무엇이겠느냐!” (1976년 1월 31일 발현)
 

 [3] Chris Ferrara, “The Third Secret of Fatima and the Post-Conciliar Debacle(파티마 제3의 비밀과 공의회 이후의 몰락)” Part 3.
 

 [4] “Open Letter to Confused Catholic(혼란에 빠진 가톨릭교회에 보내는 폭로 서한)” 레페브레(Lefebvre) 대주교, Kansas City, Angelus Press, 1992, p. 106.
 

 [5] “Wanderer(방황하는 사람)” August 31, 1967.
 

 [6] Most Reverend Aloysius Wycislo S.J., Vatican II “Revisited, Reflections by One Who Was There(다시 방문한 제2차 바티칸 공의회, 그곳에 있었던 경계 사람의 성찰)”, p. x, Alba House, Staten Island, New York; “The Devil’s Final Battle”에서 인용, p. 53.
 

 [7] Yves Congar, O.P. quoted by Father George de Nantes, CRC, no. 113, p.3.
 

 [8] Cardinal Joseph Ratzinger, “Principles of Catholic Theology(가톨릭 신학의 원리)”, Ignatius Press: San Francisco (1987) p. 42.
 

 [9] “Open Letter to Confused Catholics(혼란스러운 가톨릭 신자들에게 보내는 공개서한)”, Archbishop Lefebvre, Kansas City, Angelus Press, 1992, p. 100.
 

 [10] Ibid., p. 106.
 

 [11] Fr. Frank Poncelot, “Airwaves from Hell(지옥에서 온 전파)”, p. 187.
 

 [12] Cardinal Joseph Ratzinger, “Principles of Catholic Theology(가톨릭 신학의 원리)”, Ignatius Press: San Francisco (1987) p. 381-382.
 

 [13] Ibid., p. 334.
 

 [14] Fr. R. M. Wiltgen, “The Rhine Flows Into the Tiber(라인강이 테베레로 흐른다)”, TAN Books and Publishers (1967).
 

 [15] Fr. Frank Poncelot, “Airwaves from Hell(지옥에서 온 전파)”, pp. 143-144.
 

 [16] J. Heenan, “A Crown of Thorns(가시관)”, (London, 1974), p. 223; quoted in Latin Mass Magazine, Spring 1996, p. 45.
 

 [17] Msgr. Klaus Gamber, “The Reform of the Roman Liturgy(로마 전례의 개혁)”, p. 61.
 

 [18] Richard Cowden Guido, “John Paul II and the Battle for Vatican II(교황 요한 바오로 2세와 제2차 바티칸 공의를 위한 전투”와, Trinity Communications, 1986, the author quotes National Review, February, 1986; quoted in Fr. Frank Poncelot, “Airwaves from Hell(지옥에서 온 전파)”, pp. 18.
 

 [19] Fr. Frank Poncelot, “Airwaves from Hell(지옥에서 온 전파)”, p. 186.
 

 [20] The Devil’s Final Battle, p. 52.
 

 [21] Conference Of His Excellency Archbishop Marcel Lefebvre, Long Island, New York, 1983. 11. 5
 

 [22] Paul VI, General Audience, 1966. 1. 12
 

 [23] “Open Letter to Confused Catholics(혼란스러운 가톨릭 신자들에게 보내는 공개서한)”, Archbishop Lefebvre, Kansas City, Angelus Press, 1992, p. 107.
 

 

 

 

 [입스](https://screwslippery.com/sports/post-00029.html) 

#### 번역: 성미카엘회 회장 송 바울라 정자
 SOURCE:
 The electronic form of this document is copyrighted.
 Quotations are permissible as long as this web site is acknowledged with a hyperlink to: http://www.tldm.org
 Copyright © These Last Days Ministries, Inc. 1996 - 2022 All rights reserved.
 

 

